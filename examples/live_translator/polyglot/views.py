import contextlib

from airium import Airium


@contextlib.contextmanager
def standard_body(a: Airium, title: str = "Airium Polyglot"):
    description = "Airium Polyglot - a live server for bidirectional python-html translator."
    a("<!DOCTYPE html>")
    with a.html(lang="en"):
        with a.head():
            a.meta(charset="utf-8")
            a.meta(content="width=device-width, initial-scale=1", name="viewport")
            a.meta(content=description, name="description")
            a.title(_t=title)
            a.script("defer", type="text/javascript", src="/static/simple_md5.js")
            a.script("defer", type="text/javascript", src="/static/my_ajax.js")
        with a.body():
            yield


async def index_page():
    a = Airium()
    with standard_body(a):
        with a.content(
                style="display: flex; height: 250px; flex-direction: row;align-items: stretch; flex-wrap: wrap;"
        ):
            with pane(a):
                with text_area(a, "py-code", "python / airium"):
                    a(default_py_code)
            with pane(a):
                with text_area(a, "html-code", "HTML"):
                    a(default_html_code)
            with pane(a):
                with a.div(style="margin: 4px; height: 60px; display: flex; align-items: center;"):
                    a.h4(_t="Preview")
                    a.span(style="flex-grow: 1;")
                    with a.a(href="/last-translation"):
                        a.button(id=f"detach-btn", type="submit", _t="Open in new window")

                a.iframe(
                    id="preview-frame",
                    height="400px",
                    width="600px",
                    style="border:1px solid #888; border-radius: 2px;",
                    src="/last-translation"
                )
    return bytes(a)


@contextlib.contextmanager
def pane(a: Airium):
    with a.div(style="margin: 4px; display: flex; flex-direction: column;"):
        yield


@contextlib.contextmanager
def text_area(a, id_, title):
    with a.div(style="margin: 4px; height: 60px; display: flex; align-items: center;"):
        a.h4(_t=title)
        a.span(style="flex-grow: 1;")
        a.small(style="margin: 2px 6px;").span(id=f"{id_}-time")

    with a.textarea(id=f"{id_}-text", style="flex-grow: 1; min-width: 350px;", rows=40, spellcheck="false"):
        yield


default_py_code = """\
a('<!DOCTYPE html>')
with a.html():
    with a.body():
        a.h2(_t='HTML Images')
        a.p(_t='HTML images are defined with the img tag:')
        a.img(alt='W3Schools.com', height='142', src='https://www.w3schools.com/html/w3schools.jpg', width='104')
"""

default_html_code = """\
<!DOCTYPE html>
<html>
<body>

<h2>HTML Images</h2>
<p>HTML images are defined with the img tag:</p>

<img src="https://www.w3schools.com/html/w3schools.jpg" alt="W3Schools.com" width="104" height="142">

</body>
</html>
"""
