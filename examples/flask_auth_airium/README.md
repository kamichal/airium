# Basic flask&airium application with login

## flask_auth_airium

### Uses:

* `airium`
* `flask`
* `flask-login`
* `flask-sqlalchemy`

Bases on DigitalOcean's tutorial:
[*How To Add Authentication to Your App with
Flask-Login*](https://www.digitalocean.com/community/tutorials/how-to-add-authentication-to-your-app-with-flask-login)
by Anthony Herbert
(Last Validated on July 6, 2020)

## Differences to the original tutorial

In contrast to the tutorial, no `jinja` html templates are used. Instead of this, `airium` serves generation of the html
content.

Although this code bases on the mentioned tutorial, it slightly differs. Keeping the code for HTML generation as a
python code, thanks to `airium` it's eaiser to split views among blueprints, thus package arrangement is improved.

## Automatic install and run:

```shell
cd to_the/example/root/directory
make run
```

You can also: `make help` or `make clean`.

Then go to your browser under: http://127.0.0.1:5000/

It hosts very simple page with ability to register an account and login into it.
