"""
Basing on tutorial created by Anthony Herbert:
https://www.digitalocean.com/community/tutorials/how-to-add-authentication-to-your-app-with-flask-login
https://github.com/PrettyPrinted/flask_auth_scotch

Airium usage added by Michał Kaczmarczyk
"""

from flask import Blueprint
from flask_login import current_user, login_required

from .views import base_wrapper, index_block, profile_block

main = Blueprint("main", __name__)


@main.route("/")
def index():
    with base_wrapper() as a:
        index_block(a)
        return str(a)


@main.route("/profile")
@login_required
def profile():
    with base_wrapper() as a:
        profile_block(a, name=current_user.name)
        return str(a)
