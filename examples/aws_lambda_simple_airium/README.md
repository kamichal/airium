# Simple airium example using AWS Lambda

This is supposed to implement a serverless AWS service that returns HTML in responses.
The html is generated in python at runtime using the `airium` package.

![Airium - aws lambda architecture diagram](airium_lambda_architecture_diagram.jpg)

This example includes a `Makefile`  to automatically create a zip file containing the required
packages and the lambda function code.

You can use this zip to create the lambda function in AWS.
The resulting lambda entrypoint is `lambda_function.lambda_handler`.

You can also set up an `API Gateway` with `HTTP API` on your own.
This, however, is not required, it can be just the lambda itself.

To create a `zip`, run this command in the top directory of this example:

```bash
make build-zip
```

This should create a file containing all the packages listed in `requirements.txt` file and the
python module with lambda and airium code. For this example there is just one package: `airium`

Enjoy.

## Configuration steps in AWS Management Console

#### Build zipped environment with python packages

1. Call `make build-zip` in the example directory (in linux).
2. A new file should be created: `lambda_function_pkg.zip`

#### Create the lambda function

2. In the AWS Management Console, got to the `Lambda` service
3. Create a new function of type "Author from scratch".
4. Name it for example `AiriumExampleZippedLambda`.
5. Select the latest python as the runtime and create the lambda function.
6. In `Code source` section, upload the zip.

#### Create API Gateway

1. Go to Api Gateway and make needed connections or
2. Alternatively, you can use just the lambda.
    - Go to the function configuration -> Function URL
    - Your lambda function should be visible under the long url given, like:
      `https://jq7lmn6xpmywlwvm7m6f6xpipzy0cjebl.lambda-url.eu-north-1.on.aws/`

